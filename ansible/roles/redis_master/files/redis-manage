#!/bin/bash

name=redis-manage

EX_OK=0
EX_NOCHANGE=1
EX_ERROR=2

rhosts=("$@")
goodhosts=()
badhosts=()

retval=$EX_NOCHANGE

outlog() {
    logger -p user.info -t "$name[$$]" "$1"
    echo "$1"
}

for rhost in "${rhosts[@]}"; do
    if [[ $(redis-cli -h $rhost PING) != 'PONG' ]]; then
        outlog "$rhost is down"
        downhosts+=( $rhost )
    else
        redis-cli -h $rhost CLUSTER INFO | grep -wq 'cluster_known_nodes:1'
        if [[ $? -eq 0 ]]; then
            outlog "$rhost is lonesome"
            badhosts+=( $rhost )
        else
            if [[ $(redis-cli -h $rhost CLUSTER NODES | awk '! $9 && $3 == "myself,master"') ]]; then
                outlog "$rhost is a clustered master with no slots"
                badhosts+=( $rhost )
            else
                outlog "$rhost is good"
                goodhosts+=( $rhost )
            fi
        fi
    fi
done

if [[ ${#goodhosts[@]} -eq 0 ]]; then
    # No existing cluster, we need to create one.
    if [[ ${#badhosts[@]} -lt 3 ]]; then
        outlog "Insufficient hosts available to create a cluster!" >&2
        exit $EX_ERROR
    fi
    outlog "Creating a new cluster using ${badhosts[@]:0:3}"
    # redis-trib always requires a port, because it's dumber than redis-cli
    /bin/yes yes | redis-trib create $(for i in ${badhosts[@]:0:3}; do echo $i:6379; done)
    retval=$EX_OK
    goodhosts=(${badhosts[@]:0:3})
    badhosts=(${badhosts[@]:3})
fi

for rhost in ${goodhosts[@]}; do
    # Make sure the cluster knows about itself
    redis-cli -h ${goodhosts[0]} CLUSTER MEET $rhost 6379
done

ghost=${goodhosts[0]}
for rhost in ${badhosts[@]}; do
    outlog "Adding $rhost to the cluster using $ghost"
    loser=$(redis-cli -h $ghost CLUSTER NODES | awk '/slave [a-f0-9]{40}/{ master[$4]++ } $9 && /master -/{ master[$1]++ } END{ x=99; for (id in master) if( master[id] < x ) { x=master[id]; loser=id }; print loser }')
    redis-cli -h $ghost CLUSTER MEET $rhost 6379
    until redis-cli -h $rhost CLUSTER NODES | grep -q "^$loser"; do
        outlog "Waiting for $rhost to know about $loser"
        sleep 1
    done
    outlog "Making $rhost a replica of $loser"
    redis-cli -h $rhost CLUSTER REPLICATE $loser
    until redis-cli -h $rhost CLUSTER NODES | grep -q 'myself,slave' ; do
        outlog "Waiting for $rhost to be a replica..."
        sleep 1
    done
    ghost=$rhost
    retval=$EX_OK
done

for rhost in "${goodhosts[@]}"; do
    redis-cli -h $rhost CLUSTER NODES | while read nid nip flags junk; do
        nip=${nip%:*}
        if [[ $nip ]]; then
            echo "${rhosts[@]}" | fgrep -wq $nip
            if [[ $? -eq 1 ]]; then
                if [[ $flags = 'master' ]]; then
                    outlog "found unknown master $nip, no more forgetting"
                    exit $retval
                fi
                outlog "forgetting $nip on $rhost"
                redis-cli -h $rhost CLUSTER FORGET $nid
            fi
        else
            outlog "forgetting noaddr $nid on $rhost"
            redis-cli -h $rhost CLUSTER FORGET $nid
        fi
    done
done

exit $retval
