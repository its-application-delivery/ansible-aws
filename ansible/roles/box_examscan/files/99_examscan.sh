if [[ $USER != 'collaborate' ]]; then
    alias exam_roster_process='sudo -u collaborate /home/collaborate/examscan/exam_roster_process'
    alias exam_scans_process='sudo -u collaborate /home/collaborate/examscan/exam_scans_process'
fi
